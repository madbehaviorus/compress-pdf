#/bin/bash
#
# madbehaviorus 
# 
# 2021-11-14
# v0.1
# 
# this script is for compress local pdf files

# variables
path=$1
comp=$2;
ein=$3;

i=1;
temp=$(echo "$path" | tail -c 2)


# help
if [ $path == "-h" ]
then
    echo ""
    echo ""
    echo "Help:"
    echo ""
    echo "1. The first command is for the exacty path to the directory, in there are the *.html files. "
    echo "2. Second command is for the compression rate."
    echo "   You can choose five different rates (ebook is the standard):"
    echo ""
    echo "      (1) = screen   (screen-view-only quality, 72 dpi images)"
    echo "      (2) = ebook    (low quality, 150 dpi images)  standard" 
    echo "      (3) = printer  (high quality, 300 dpi images)"
    echo "      (4) = prepress (high quality, color preserving, 300 dpi imgs)"
    echo "      (5) = default  (almost identical to /screen)"
    echo ""
    echo "3. If you want, insert the the maximum (\$max > 0) of this html files, do you want to convert. "
    echo "   Default = all files in the directory."
    echo ""
    echo "like:"
    echo "convert-to-pdf \$path \$compression"
    echo ""
    echo "or:"
    echo "convert-to-pdf \$path \$compression \$max"
    echo ""
    echo "" 
    exit
fi

# test installed gs
if [ ! -f /usr/bin/gs ] 
then
    echo "Please install gs." 
    exit
fi


# test path
if [ $temp == "\\" ]
then
    ls -a $path*.pdf > books.txt
else
    if [ -d $path ]
    then
        ls -a $path/*.pdf > books.txt
    else
        echo "Path does not exist!";
        exit;
    fi
fi



# looking for the inserted sites
if [[ $ein =~ ^-?[0-9]+$ ]] && (( $ein > 1 ))
then
    echo "ok"
else
    ein=$(cat books.txt | wc -l)
fi


# looking for compression rate
if [[ $comp =~ ^-?[0-9]+$ ]] && (( $comp > 0 ))
then
    case $comp in
        1) 
            comp="screen"
            ;;
        2) 
            comp="ebook"
            ;;
        3) 
            comp="printer"
            ;;
        4) 
            comp="prepress"
            ;;
        5) 
            comp="default"
            ;;
        *)
            comp="ebook"
            ;;       
    esac
    
    echo ""
    echo "Set compression rate to: $comp".
    echo ""
    echo ""
else
    comp="ebook";
fi

# the convert commands
while (( $i <= $ein ))
do
    page=$(cat books.txt | head -n $i | tail -n 1 );
    echo "Convert: "  $page;
    temp=${#page}
    temp=$(($temp-"3"))
    temp=$(echo $page | head -c $temp)
    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$temp-comp.pdf "$page"
    echo "Success: $temp-comp.pdf";
    echo ""
    echo ""
    ((i++));
done


# rm list
rm books.txt

echo ""
echo ""
echo "Done!";
echo ""
echo ""



