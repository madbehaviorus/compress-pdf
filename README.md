


# compress-pdf...

is a script who use [gs](https://manpages.debian.org/jessie/ghostscript/gs.1.en.html) to compress not only one file, but all files in one directory

## Help:


    1. The first command is for the exacty path to the directory, in there are the *.html files.     
    2. Second command is for the compression rate.    
       You can choose six different rates (ebook is the standard):    
        
          (1) = screen   (screen-view-only quality, 72 dpi images)    
          (2) = ebook    (low quality, 150 dpi images)  standard     
          (3) = printer  (high quality, 300 dpi images)    
          (4) = prepress (high quality, color preserving, 300 dpi imgs)    
          (5) = default  (almost identical to /screen)    
        
    3. If you want, insert the the maximum (\$max > 0) of this html files, do you want to convert.     
       Default = all files in the directory.    
        
    like:    
    convert-to-pdf \$path \$compression    
        
    or:    
    convert-to-pdf \$path \$compression \$max    
        
         

